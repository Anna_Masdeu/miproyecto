from FigurasL import*


#cuadrado
altura = float(input("Escribe una altura "))
coordenada1= int(input("Escribe coordenada x "))
coordenada2= int(input("Escribe coordenada y "))


cuadrado1=Cuadrado(altura,Coordenada(coordenada1, coordenada2))
#print(cuadrado1.getCoordenada().getX()) funciona
#print(cuadrado1.getCoordenada().getY()) funcion


#print(cuadrado1.getCoordenada().__str__())   no hace falta __str__ porque ya tiene return definido en su funcion
#ya existe una funcion que devuelve las coordenadas: __str__()
print(cuadrado1.getCoordenada())


#triangulo
altura = float(input("Escribe una altura "))
base = float(input("EScriba la base "))
coordenada1= int(input("Escribe coordenada x "))
coordenada2= int(input("Escribe coordenada y "))


triangulo1=Triangulo(altura,base,Coordenada(coordenada1,coordenada2))
print(triangulo1.getCoordenada())


#circulo

radio=float(input("Escribe el radio "))
coordenada1= int(input("Escribe coordenada x "))
coordenada2= int(input("Escribe coordenada y "))

circulo1=Circulo(radio,Coordenada(coordenada1,coordenada2))
print(circulo1.getCoordenada())

